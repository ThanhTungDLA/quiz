//
//  main.m
//  AppSimple
//
//  Created by iViettech on 5/20/14.
//  Copyright (c) 2014 iViettech. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
