//
//  AppDelegate.h
//  AppSimple
//
//  Created by iViettech on 5/20/14.
//  Copyright (c) 2014 iViettech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
